import requests
import os

CI_PROJECT_ID = os.environ["CI_PROJECT_ID"]
CI_API_V4_URL = os.environ["CI_API_V4_URL"]
ACCESS_TOKEN = os.environ["ACCESS_TOKEN"]
BASE_DOMAIN = os.environ["BASE_DOMAIN"]


def listProjects():
    projects = os.listdir("./projects")
    print(projects)
    return projects


def listEnvironments():
    response = requests.get(
        f"{CI_API_V4_URL}/projects/{CI_PROJECT_ID}/environments",
        headers={"PRIVATE-TOKEN": f"{ACCESS_TOKEN}"},
    )
    return response.json()


def registerEnvironment(name: str):
    response = requests.post(
        f"{CI_API_V4_URL}/projects/{CI_PROJECT_ID}/environments",
        data={"name": name, "external_url": f"https://{name}.{BASE_DOMAIN}"},
        headers={"PRIVATE-TOKEN": f"{ACCESS_TOKEN}"},
    )
    return response.json()


def deleteEnvironment(environment_id: int):
    response = requests.delete(
        f"{CI_API_V4_URL}/projects/{CI_PROJECT_ID}/environments/{environment_id}",
        headers={"PRIVATE-TOKEN": f"{ACCESS_TOKEN}"},
    )
    return response.json()

def main():
    # Get list of projects
    projects = listProjects()

    # Get list of environments
    environments = listEnvironments()

    # Delete environments that are not in the list of projects
    for environment in environments:
        if environment["name"] not in projects:
            print(f"Deleting environment {environment['name']}")
            deleteEnvironment(environment["id"])

    # Register environments that are not in the list of environments
    for project in projects:
        if project not in [environment["name"] for environment in environments]:
            print(f"Registering environment {project}")
            registerEnvironment(project)
    

if __name__ == "__main__":
    main()
